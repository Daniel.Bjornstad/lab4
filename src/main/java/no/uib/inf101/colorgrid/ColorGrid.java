package no.uib.inf101.colorgrid;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{
  private int cols;
  private int rows;
  private Color[][] grid;

  public ColorGrid(int rows, int cols){
    this.rows = rows;
    this.cols = cols;
    this.grid = new Color[rows][cols];
    }

  @Override
  public Color get(CellPosition pos) {
    checkBounds(pos);
    return grid[pos.row()][pos.col()];  
  }

  @Override
  public void set(CellPosition pos, Color color) {
    checkBounds(pos);
    grid[pos.row()][pos.col()] = color;
        }

  @Override
  public int rows() {
      return rows;
  }

  @Override
  public int cols() {
      return cols;
  }

  @Override
    public List<CellColor> getCells() {
      List<CellColor> cells = new ArrayList<>();
      for (int row = 0; row < rows; row++) {
          for (int col = 0; col < cols; col++) {
              CellPosition pos = new CellPosition(row, col);
              Color color = grid[row][col];
              cells.add(new CellColor(pos, color));
      
          }
      }
      return cells;
    }

    private void checkBounds(CellPosition pos) {
      if (pos.row() < 0 || pos.row() >= rows || pos.col() < 0 || pos.col() >= cols) {
          throw new IndexOutOfBoundsException("Position out of bounds: " + pos);
}
}
  }