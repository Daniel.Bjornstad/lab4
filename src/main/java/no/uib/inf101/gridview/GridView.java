package no.uib.inf101.gridview;
import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;

import javax.swing.JPanel;

public class GridView extends JPanel {
  private IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid){
    this.colorGrid = colorGrid;
    this.setPreferredSize(new Dimension(400, 300));
  }
  @Override
  protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      drawGrid(g2);
  }

  private void drawGrid(Graphics2D G2D) {
    double x = OUTERMARGIN;
    double y = OUTERMARGIN;
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Rectangle2D rectangle2d = new Rectangle2D.Double(x, y, width, height);

    G2D.setColor(MARGINCOLOR);
    G2D.fill(rectangle2d);

    CellPositionToPixelConverter cellConverter = new CellPositionToPixelConverter(getBounds(), colorGrid, OUTERMARGIN);
    drawCells(G2D, colorGrid,cellConverter);
  }

  private static void drawCells(Graphics2D G2D, CellColorCollection colorCollection, CellPositionToPixelConverter cellConverter) {
    List<CellColor> cells = colorCollection.getCells();

    for (CellColor cellColor : cells) {
      CellPosition cellPosition = cellColor.cellPosition();
      Color color;
      if (cellColor.color() != null) {
          color = cellColor.color();
      } else {
          color = Color.DARK_GRAY;
      }
      G2D.setColor(color);
      G2D.fill(cellConverter.getBoundsForCell(cellPosition));
    }
    }
  }
  
